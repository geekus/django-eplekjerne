import urlparse

from django.contrib.auth.models import User

from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer

from tastypie.api import Api

class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded'
    }

    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self, content):
        pass

class EpleResourceMeta:
    always_return_data = True
    authorization = Authorization()
    serializer = urlencodeSerializer()

from eplekjerne.contents.api import ContentResource
from eplekjerne.contents.files.api import FileResource
from eplekjerne.contents.images.api import ImageResource
from eplekjerne.contents.posts.api import PostResource

from eplekjerne.modules.contentlists.api import ContentlistResource
from eplekjerne.modules.slideshows.api import SlideshowResource

from eplekjerne.fetchers.api import FetcherResource

from eplekjerne.tags.api import TagResource
from eplekjerne.sections.api import SectionResource

from eplekjerne.geos.pois.api import PoiResource
from eplekjerne.geos.areas.api import AreaResource


### Set up Tastypie URL's ###

eplekjerne_api = Api(api_name='v1')

eplekjerne_api.register(ContentResource())
eplekjerne_api.register(FileResource())
eplekjerne_api.register(ImageResource())
eplekjerne_api.register(PostResource())

eplekjerne_api.register(ContentlistResource())
eplekjerne_api.register(SlideshowResource())

eplekjerne_api.register(FetcherResource())

eplekjerne_api.register(SectionResource())
eplekjerne_api.register(TagResource())

eplekjerne_api.register(PoiResource())
eplekjerne_api.register(AreaResource())
