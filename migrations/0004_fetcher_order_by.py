# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eplekjerne', '0003_fetcher_content_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='fetcher',
            name='order_by',
            field=models.CharField(default='', max_length=255, blank=True),
            preserve_default=False,
        ),
    ]
