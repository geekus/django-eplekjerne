# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.CharField(max_length=255)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('publish_start', models.DateTimeField(null=True, blank=True)),
                ('publish_end', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Fetcher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('limit', models.IntegerField(max_length=10, null=True)),
                ('offset', models.IntegerField(max_length=10, null=True)),
                ('order', models.CharField(max_length=255, blank=True)),
                ('reorder', models.CommaSeparatedIntegerField(max_length=255, blank=True)),
                ('from_date', models.DateTimeField(null=True)),
                ('to_date', models.DateTimeField(null=True)),
            ],
            options={
                'db_table': 'eplekjerne_fetchers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Content')),
                ('name', models.CharField(max_length=255, blank=True)),
                ('file', models.FileField(upload_to=b'.')),
            ],
            options={
                'db_table': 'eplekjerne_contents_files',
            },
            bases=('eplekjerne.content',),
        ),
        migrations.CreateModel(
            name='Geo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
                'db_table': 'eplekjerne_geos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Area',
            fields=[
                ('geo_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Geo')),
                ('geojson', django.contrib.gis.db.models.fields.PolygonField(srid=4326)),
            ],
            options={
                'db_table': 'eplekjerne_geos_areas',
            },
            bases=('eplekjerne.geo',),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Content')),
                ('center', models.CharField(max_length=255, blank=True)),
                ('image', models.ImageField(max_length=255, upload_to=b'images/original')),
                ('filename', models.CharField(max_length=255, blank=True)),
                ('height', models.IntegerField(null=True)),
                ('width', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'eplekjerne_contents_images',
            },
            bases=('eplekjerne.content',),
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('slug', models.CharField(max_length=255, blank=True)),
                ('description', models.TextField(blank=True)),
                ('config', models.TextField(blank=True)),
                ('push', models.BooleanField(default=False)),
                ('pull', models.BooleanField(default=False)),
                ('static', models.BooleanField(default=False)),
                ('attr_class', models.CharField(max_length=255, blank=True)),
            ],
            options={
                'db_table': 'eplekjerne_modules',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contentview',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
            ],
            options={
                'db_table': 'eplekjerne_modules_contentviews',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.CreateModel(
            name='Contentlist',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
            ],
            options={
                'db_table': 'eplekjerne_modules_contentlists',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.CreateModel(
            name='Code',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
                ('code', models.TextField()),
            ],
            options={
                'db_table': 'eplekjerne_modules_codes',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.CreateModel(
            name='ModuleHasContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'eplekjerne_modules_has_contents',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ModuleHasSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'eplekjerne_modules_has_sections',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ModuleHasTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'eplekjerne_modules_has_tags',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Navigation',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
            ],
            options={
                'db_table': 'eplekjerne_modules_navigations',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Content')),
                ('title', models.CharField(max_length=255, blank=True)),
                ('body', models.TextField(blank=True)),
                ('parent_id', models.IntegerField(null=True)),
                ('lft', models.IntegerField()),
                ('rght', models.IntegerField()),
            ],
            options={
                'db_table': 'eplekjerne_contents_pages',
            },
            bases=('eplekjerne.content',),
        ),
        migrations.CreateModel(
            name='Poi',
            fields=[
                ('geo_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Geo')),
                ('geojson', django.contrib.gis.db.models.fields.PointField(srid=4326)),
            ],
            options={
                'db_table': 'eplekjerne_geos_pois',
            },
            bases=('eplekjerne.geo',),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('content_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Content')),
                ('title', models.CharField(max_length=255, blank=True)),
                ('lead', models.TextField()),
                ('body', models.TextField()),
            ],
            options={
                'db_table': 'eplekjerne_contents_posts',
            },
            bases=('eplekjerne.content',),
        ),
        migrations.CreateModel(
            name='Relatedcontent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(null=True)),
                ('relationtype', models.TextField(blank=True)),
                ('related_content', models.ForeignKey(related_name=b'related_content', to='eplekjerne.Content')),
                ('related_from', models.ForeignKey(related_name=b'related_from', to='eplekjerne.Content')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
                ('slug', models.CharField(max_length=255)),
                ('path', models.CharField(max_length=255)),
                ('parent_id', models.IntegerField(null=True)),
                ('c_lft', models.IntegerField(null=True)),
                ('c_rght', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'eplekjerne_sections',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sitearea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('order', models.IntegerField(null=True, blank=True)),
                ('default', models.BooleanField(default=False)),
                ('slug', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'db_table': 'eplekjerne_siteareas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SiteareaHasSitelayout',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.ForeignKey(blank=True, to='eplekjerne.Content', null=True)),
                ('section', models.ForeignKey(blank=True, to='eplekjerne.Section', null=True)),
                ('sitearea', models.ForeignKey(to='eplekjerne.Sitearea')),
            ],
            options={
                'db_table': 'eplekjerne_siteareas_has_sitelayouts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sitelayout',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('slug', models.CharField(max_length=255, blank=True)),
            ],
            options={
                'db_table': 'eplekjerne_sitelayouts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SitelayoutHasModule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'eplekjerne_sitelayouts_has_modules',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('files', models.ManyToManyField(related_name=b'+', db_table=b'eplekjerne_slides_has_contents', to='eplekjerne.File')),
            ],
            options={
                'db_table': 'eplekjerne_modules_slideshows_slides',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slideshow',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
                ('files', models.ManyToManyField(related_name=b'slideshows', db_table=b'eplekjerne_slideshows_has_contents', to='eplekjerne.File')),
            ],
            options={
                'db_table': 'eplekjerne_modules_slideshows',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'eplekjerne_tags',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Weather',
            fields=[
                ('module_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='eplekjerne.Module')),
            ],
            options={
                'db_table': 'eplekjerne_modules_weathers',
            },
            bases=('eplekjerne.module',),
        ),
        migrations.AddField(
            model_name='slide',
            name='slideshow',
            field=models.ForeignKey(to='eplekjerne.Slideshow'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitelayouthasmodule',
            name='module',
            field=models.ForeignKey(to='eplekjerne.Module'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitelayouthasmodule',
            name='sitelayout',
            field=models.ForeignKey(to='eplekjerne.Sitelayout'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitelayout',
            name='modules',
            field=models.ManyToManyField(to='eplekjerne.Module', through='eplekjerne.SitelayoutHasModule'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='siteareahassitelayout',
            name='sitelayout',
            field=models.ForeignKey(to='eplekjerne.Sitelayout'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitearea',
            name='layout',
            field=models.ManyToManyField(related_name=b'contents', through='eplekjerne.SiteareaHasSitelayout', to='eplekjerne.Sitelayout'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehastag',
            name='module',
            field=models.ForeignKey(to='eplekjerne.Module'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehastag',
            name='tag',
            field=models.ForeignKey(to='eplekjerne.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehassection',
            name='module',
            field=models.ForeignKey(to='eplekjerne.Module'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehassection',
            name='section',
            field=models.ForeignKey(to='eplekjerne.Section'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehascontent',
            name='module',
            field=models.ForeignKey(to='eplekjerne.Module'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modulehascontent',
            name='tag',
            field=models.ForeignKey(to='eplekjerne.Content'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='module',
            name='content',
            field=models.ManyToManyField(related_name=b'modules', null=True, through='eplekjerne.ModuleHasContent', to='eplekjerne.Content'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='module',
            name='fetcher',
            field=models.ForeignKey(to='eplekjerne.Fetcher', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='module',
            name='section',
            field=models.ForeignKey(to='eplekjerne.Section', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='module',
            name='sections',
            field=models.ManyToManyField(related_name=b'modules', null=True, through='eplekjerne.ModuleHasSection', to='eplekjerne.Section'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='module',
            name='tags',
            field=models.ManyToManyField(related_name=b'modules', null=True, through='eplekjerne.ModuleHasTag', to='eplekjerne.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fetcher',
            name='tags',
            field=models.ManyToManyField(related_name=b'fetchers', db_table=b'eplekjerne_fetchers_has_tags', to='eplekjerne.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='content',
            name='contents',
            field=models.ManyToManyField(related_name=b'related_to', through='eplekjerne.Relatedcontent', to='eplekjerne.Content'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='content',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='content',
            name='section',
            field=models.ForeignKey(blank=True, to='eplekjerne.Section', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='content',
            name='sections',
            field=models.ManyToManyField(related_name=b'contents', db_table=b'eplekjerne_sections_has_contents', to='eplekjerne.Section'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='content',
            name='tags',
            field=models.ManyToManyField(related_name=b'contents', db_table=b'eplekjerne_tagged_contents', to='eplekjerne.Tag'),
            preserve_default=True,
        ),
    ]
