# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eplekjerne', '0004_fetcher_order_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='fetcher',
            name='section',
            field=models.ForeignKey(blank=True, to='eplekjerne.Section', null=True),
            preserve_default=True,
        ),
    ]
