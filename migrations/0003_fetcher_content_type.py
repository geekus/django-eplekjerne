# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eplekjerne', '0002_image_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='fetcher',
            name='content_type',
            field=models.CharField(default='Image', max_length=255, choices=[(b'Bilde', b'Image'), (b'Post', b'Post'), (b'Side', b'Page')]),
            preserve_default=False,
        ),
    ]
