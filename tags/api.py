from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.tags.models import Tag
from eplekjerne.api import EpleResourceMeta

class TagResource(ModelResource):

    # contents = fields.ToManyField('eplekjerne.contents.api.ContentResource', 'contents', null=True)

    class Meta(EpleResourceMeta):
        queryset = Tag.objects.all()
        resource_name = 'tag'
        filtering = { "title" : ALL }
