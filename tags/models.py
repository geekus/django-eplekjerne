from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *

class Tag(models.Model):

    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    # contents = models.ManyToManyField('eplekjerne.contents.models.Content', through="TaggedContent", related_name="tags")

    class Meta(EpleMeta):
        db_table = 'eplekjerne_tags'
        pass

    def save(self, *args, **kwargs):
        # For automatic slug generation.
        if not self.slug:
            self.slug = slugify(self.title)[:50]

        return super(Tag, self).save(*args, **kwargs)
