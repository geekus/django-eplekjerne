from django.db import models

class EpleMeta:
    app_label = 'eplekjerne'

from eplekjerne.tags.models import *

from eplekjerne.contents.models import *
from eplekjerne.contents.files.models import *
from eplekjerne.contents.images.models import *
from eplekjerne.contents.pages.models import *
from eplekjerne.contents.posts.models import *

from eplekjerne.modules.models import *
from eplekjerne.modules.codes.models import *
from eplekjerne.modules.contentlists.models import *
from eplekjerne.modules.contentviews.models import *
from eplekjerne.modules.navigations.models import *
from eplekjerne.modules.slideshows.models import *
from eplekjerne.modules.weather.models import *

from eplekjerne.fetchers.models import *
from eplekjerne.siteareas.models import *
from eplekjerne.sitelayouts.models import *
# from eplekjerne.templates.models import *

from eplekjerne.sections.models import *

from eplekjerne.geos.pois.models import *
from eplekjerne.geos.areas.models import *
