from django.db import models

from eplekjerne.models import *
from eplekjerne.contents.models import *

class File(Content):

    name = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to='.')

    class Meta(EpleMeta):
        db_table = 'eplekjerne_contents_files'

    def save(self, *args, **kwargs):
        # For automatic slug generation.
        if not self.slug:
            # self.slug = slugify(self.name)[:50]
            self.slug = 'superfil'

        return super(File, self).save(*args, **kwargs)



    def __unicode__(self):
        return self.name
