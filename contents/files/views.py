# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# from myproject.myapp.models import Document
from eplekjerne.contents.files.models import File
# from myproject.myapp.forms import DocumentForm

def list(request):
    # Handle file upload
    if request.method == 'POST':
        # form = DocumentForm(request.POST, request.FILES)
        # if form.is_valid():
        newdoc = File(file = request.FILES['files[]'])
        newdoc.save()

        # Redirect to the document list after POST
        return HttpResponseRedirect(reverse('eplekjerne.contents.files.views.list'))
    else:
        # form = DocumentForm() # A empty, unbound form
        pass

    # Load documents for the list page
    documents = File.objects.all()

    # Render list page with the documents and the form
    # return render_to_response(
    #     'myapp/list.html',
    #     {'documents': documents, 'form': form},
    #     context_instance=RequestContext(request)
    # )
