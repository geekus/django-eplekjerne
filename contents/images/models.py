from __future__ import division # NOTE: Remove this in Python 3

import re
import os

from django.db import models

from eplekjerne.models import *
from eplekjerne.contents.models import *

class Image(Content):

    center = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to='images/original', max_length=255)
    filename = models.CharField(max_length=255, blank=True)
    height = models.IntegerField(null=True)
    width = models.IntegerField(null=True)
    description = models.TextField(blank=True, default='')

    def _get_aspect_ratio(self):
        aspect_ratio = self.width / self.height
        # print(aspect_ratio)
        return aspect_ratio

    aspect_ratio = property(_get_aspect_ratio)

    # def _get_filename(self):
    #     head, tail = os.path.split(self.image)
    #     return tail

    # filename = property(_get_filename)

    # @property
    # def filename(self):
    #     filename = re.search('(\w+\.\w+)[^/]*$', self.image)
    #     # filename = self.image
    #     filename = filename.group(0)
    #     return filename

    class Meta(EpleMeta):
        db_table = 'eplekjerne_contents_images'

    def save(self, *args, **kwargs):

        # print self.image.name
        print slugify(self.image.name)[:50]
        # For automatic slug generation.
        if not self.slug:
            self.slug = slugify(self.image.name)[:50]

        if not self.filename:
            path, filename = os.path.split(self.image.path)
            self.filename = filename

        return super(Image, self).save(*args, **kwargs)


    # def delete(self, *args, **kwargs):
    #     super(Image, self).delete(*args, **kwargs) # Call the "real" delete() method.
