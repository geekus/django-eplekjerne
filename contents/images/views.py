# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.files.images import get_image_dimensions

# For JSON
import json
from django.utils.functional import Promise
from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder

# from myproject.myapp.models import Document
from eplekjerne.contents.images.models import Image
# from myproject.myapp.forms import DocumentForm

def upload(request):
    # Handle file upload
    if request.method == 'POST':
        # form = DocumentForm(request.POST, request.FILES)
        # if form.is_valid():

        picture = request.FILES['files[]']
        w, h = get_image_dimensions(picture)

        newdoc = Image(image = request.FILES['files[]'], width = w, height = h)
        newdoc.save()

        image_id = newdoc.id

        response_data = {}
        response_data['id'] = image_id
        return HttpResponse(json.dumps(response_data), content_type="application/json", status=201)

        # Redirect to the document list after POST
        # return HttpResponseRedirect(reverse('eplekjerne.contents.files.views.list'))
        # return HttpResponse("Here's the text of the Web page.")

    else:
        # form = DocumentForm() # A empty, unbound form
        # pass
        return HttpResponse("Nathing to see herrre...")

    # Load documents for the list page
    # documents = File.objects.all()

    # Render list page with the documents and the form
    # return render_to_response(
    #     'myapp/list.html',
    #     {'documents': documents, 'form': form},
    #     context_instance=RequestContext(request)
    # )
