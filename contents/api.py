from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.contents.models import *
from eplekjerne.api import EpleResourceMeta
from eplekjerne.sections.api import SectionResource

class ContentResource(ModelResource):

    # sections = fields.ToManyField(SectionResource, 'sections', null=True, blank=True, related_name='contents')
    section = fields.ToOneField('eplekjerne.sections.api.SectionResource', 'section', null=True, blank=True)
    sections = fields.ToManyField('eplekjerne.sections.api.SectionResource', 'sections', null=True, blank=True, related_name='contents')

    # M2M
    tags = fields.ToManyField('eplekjerne.tags.api.TagResource', 'tags', null=True, blank=True, related_name='contents')

    # sections = fields.ToManyField(
    #     SectionHasContentResource,
    #     attribute=lambda bundle: bundle.obj.sections.through.objects.filter(content=bundle.obj) or bundle.obj.sections,
    #     full=True
    # )

    # sections = fields.ToManyField(
    #     SectionHasContentResource,
    #     # attribute=lambda bundle: bundle.obj.sections.through.objects.filter(content=bundle.obj) or bundle.obj.sections,
    #     attribute='sections',
    #     related_name='section',
    #     full=True
    # )

    # def hydrate(self, bundle):
    #     # Don't change existing slugs.
    #     # In reality, this would be better implemented at the ``Note.save``
    #     # level, but is for demonstration.
    #     # if not bundle.obj.pk:
    #     #     bundle.obj.slug = slugify(bundle.data['title'])

    #     print 'Hydrate'
    #     print bundle
    #     # print bundle.request

    #     return bundle

    class Meta(EpleResourceMeta):
        queryset = Content.objects.all()
        resource_name = "content"


# class SectionHasContentResource(ModelResource):
#     # content = fields.ToOneField(ContentResource, 'content', full=True)
#     section = fields.ToOneField(SectionResource, 'section', full=True)

#     class Meta(EpleMeta):
#         queryset= SectionHasContent.objects.all()

# class RecipeResource(ModelResource):
#     ingredients = fields.ToManyField(RecipeIngredientResource,
#             attribute=lambda bundle: bundle.obj.ingredients.through.objects.filter(
#                 recipe=bundle.obj) or bundle.obj.ingredients, full=True)
#     class Meta(EpleMeta):
#         queryset = Recipe.objects.all()
#         resource_name = 'recipe'
