from django.db import models

from eplekjerne.models import *
from eplekjerne.contents.models import *

class Page(Content):

    title = models.CharField(max_length=255, blank=True)
    body = models.TextField(blank=True)
    parent_id = models.IntegerField(null=True)
    lft = models.IntegerField()
    rght = models.IntegerField()

    class Meta(EpleMeta):
        db_table = 'eplekjerne_contents_pages'
        pass

    def __unicode__(self):
        return self.title
