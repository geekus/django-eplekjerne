from django.db import models
from django.utils.text import slugify

from django.contrib.auth.models import User

from eplekjerne.models import *
from eplekjerne.sections.models import *

class Content(models.Model):

    slug = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    publish_start = models.DateTimeField(blank=True, null=True)
    publish_end = models.DateTimeField(blank=True, null=True)
    section = models.ForeignKey('Section', blank=True, null=True)
    sections = models.ManyToManyField('Section', related_name='contents', db_table='eplekjerne_sections_has_contents')
    # sections = models.ManyToManyField(Section, through="SectionHasContent", related_name="contents")
    tags = models.ManyToManyField('Tag', related_name='contents', db_table='eplekjerne_tagged_contents')
    # tags = models.ManyToManyField(Tag, through="TaggedContent", related_name="contents")
    contents = models.ManyToManyField('self', through='Relatedcontent', symmetrical=False, related_name='related_to')
    created_by = models.ForeignKey(User, null=True)

    class Meta(EpleMeta):
        pass

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.id

    def save(self, *args, **kwargs):
        # For automatic slug generation.
        if not self.slug:
            self.slug = slugify(self.title)[:50]

        # import logging
        # logger = logging.getLogger('bonesmag.dlogger')
        # logger.debug('Saving!!!')

        # print 'Saving!'
        # print args
        # print kwargs
        # tags = self.tags

        # for tag in tags:
        #     print tag.id
        #     print tag.title

        return super(Content, self).save(*args, **kwargs)


class Relatedcontent(models.Model):

    related_from = models.ForeignKey('Content', related_name='related_from')
    related_content = models.ForeignKey('Content', related_name='related_content')
    order = models.IntegerField(null=True)
    relationtype = models.TextField(blank=True)

    class Meta(EpleMeta):
        pass
