from django.db import models

from eplekjerne.models import *
from eplekjerne.contents.models import *

class Post(Content):

    title = models.CharField(max_length=255, blank=True)
    lead = models.TextField()
    body = models.TextField()

    class Meta(EpleMeta):
        db_table = 'eplekjerne_contents_posts'
        pass

    def __unicode__(self):
        return self.title
