import urlparse

# from django.contrib.auth.models import User
# from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.api import EpleResourceMeta
from eplekjerne.contents.api import ContentResource
from eplekjerne.contents.posts.models import Post


class PostResource(ContentResource):

    pass

    class Meta(EpleResourceMeta):
        queryset = Post.objects.all()
        resource_name = 'post'
        filtering = {'title' : ALL}
        # resource_name = 'bookmarklet_post'
        # include_resource_uri = False
        # limit = 10
        # default_format = "application/json"
        # object_class = ProxyStore
        # authorization = Authorization()
