from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.sections.models import Section
from eplekjerne.api import EpleResourceMeta

class SectionResource(ModelResource):

    contents = fields.ToManyField('eplekjerne.contents.api.ContentResource', 'contents', null=True)

    # contents = fields.ToManyField(SectionHasContentResource,
    #     attribute=lambda bundle: bundle.obj.contents.through.objects.filter(
    #         section=bundle.obj) or bundle.obj.contents, full=True)

    # TRY THIS
    # sections = fields.ToManyField(
    #     SectionResource,
    #     attribute=lambda bundle: Section.objects.filter(sections=bundle.obj),
    #     related_name='sections',
    #     full=True
    # )

    class Meta(EpleResourceMeta):
        queryset = Section.objects.all()
        resource_name = 'section'
        filtering = { "title" : ALL }


# class SectionResource(ModelResource):

#     class Meta(EpleResourceMeta):
#         queryset = Section.objects.all()
#         resource_name = 'section'
#         filtering = { "title" : ALL }
#         authorization = Authorization()
#         # resource_name = 'bookmarklet_post'
#         # include_resource_uri = False
#         # limit = 10
#         # default_format = "application/json"
#         # object_class = ProxyStore
#         # authorization = Authorization()
#         serializer = urlencodeSerializer()
