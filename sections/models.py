from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *

class Section(models.Model):

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    slug = models.CharField(max_length=255)
    path = models.CharField(max_length=255) # Cached path
    parent_id = models.IntegerField(null=True)
    c_lft = models.IntegerField(null=True)
    c_rght = models.IntegerField(null=True)

    class Meta(EpleMeta):
        db_table = 'eplekjerne_sections'
