from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *
from eplekjerne.contents.models import *
# from eplekjerne.contenttypes.models import *
# from eplekjerne.moduletypes.models import *
from eplekjerne.sections.models import *
from eplekjerne.fetchers.models import *
from eplekjerne.tags.models import *

class Module(models.Model):

    title = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    config = models.TextField(blank=True)
    # moduletype = models.ForeignKey('Moduletype')
    # contenttype = models.ForeignKey('Contenttype')
    section = models.ForeignKey('Section', null=True)
    push = models.BooleanField(default=False)
    pull = models.BooleanField(default=False)
    static = models.BooleanField(default=False)
    attr_class = models.CharField(max_length=255, blank=True)
    sections = models.ManyToManyField('Section', through='ModuleHasSection', related_name='modules', null=True)
    tags = models.ManyToManyField('Tag', through='ModuleHasTag', related_name='modules', null=True)
    content = models.ManyToManyField('Content', through='ModuleHasContent', related_name='modules', null=True)
    fetcher = models.ForeignKey('Fetcher', null=True)

    @property
    def contents(self):
        # if self.content:
        #     return self.content
        # else:
        return self.fetcher.contents


    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules'



class ModuleHasSection(models.Model):

    module = models.ForeignKey('Module')
    section = models.ForeignKey('Section')
    order = models.IntegerField(null=True)

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_has_sections'



class ModuleHasTag(models.Model):

    module = models.ForeignKey('Module')
    tag = models.ForeignKey('Tag')

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_has_tags'



class ModuleHasContent(models.Model):

    module = models.ForeignKey('Module')
    tag = models.ForeignKey('Content')

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_has_contents'
