from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *
from eplekjerne.modules.models import *

class Code(Module):
    code = models.TextField()

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_codes'
