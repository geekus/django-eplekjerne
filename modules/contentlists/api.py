import urlparse

from django.conf.urls import patterns, include, url

from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.api import EpleResourceMeta
from eplekjerne.modules.api import ModuleResource
from eplekjerne.modules.contentlists.models import Contentlist


class ContentlistResource(ModuleResource):

    contents = fields.ListField(readonly=True, null=True, blank=True, attribute="contents")

    class Meta(EpleResourceMeta):
        queryset = Contentlist.objects.all()
        resource_name = 'list'
        filtering = {'title': ALL}

    def dehydrate(self, bundle):
        uris = []
        # filenames = []

        contents = bundle.data['contents']

        if contents:
            for content in contents:
                content_uri = '/api/v1/content/' + str(content.id) + '/'
                uris.append(str(content_uri))
                # content_file = str(content.data['content'])
                # filenames.append(content_file)
            bundle.data['contents'] = uris
            # bundle.data['filenames'] = filenames

        return bundle
