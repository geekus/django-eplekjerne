from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *
from eplekjerne.modules.models import *

class Slideshow(Module):

    files = models.ManyToManyField('File', related_name='slideshows', db_table='eplekjerne_slideshows_has_contents')

    def get_promo_image(self):
        promo_image = self.contents.first()
        return promo_image

    promo_image = property(get_promo_image)

    def _get_filenames(self):
        filenames = self.contents.values_list('filename', flat=True)
        return filenames

    filenames = property(_get_filenames)

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_slideshows'



class Slide(models.Model):

    files = models.ManyToManyField('File', related_name='+', db_table='eplekjerne_slides_has_contents')
    # layout = models.ForeignKey('Layout')
    slideshow = models.ForeignKey('Slideshow')

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_slideshows_slides'
