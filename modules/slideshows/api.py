import urlparse

from django.conf.urls import patterns, include, url

from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.api import EpleResourceMeta
from eplekjerne.modules.api import ModuleResource
from eplekjerne.modules.slideshows.models import Slideshow


class SlideshowResource(ModuleResource):

    # contents = fields.ToManyField('eplekjerne.contents.api.ContentResource', 'contents', readonly=True, null=True, blank=True, full=True)
    contents = fields.ListField(readonly=True, null=True, blank=True, attribute="contents")
    promo_image = fields.ForeignKey('eplekjerne.contents.images.api.ImageResource', 'promo_image', readonly=True, null=True, blank=True, full=False)
    filenames = fields.ListField(attribute='filenames', readonly=True, blank=True, null=True)

    class Meta(EpleResourceMeta):
        queryset = Slideshow.objects.all()
        resource_name = 'slideshow'
        filtering = {'title': ALL}
        # resource_name = 'bookmarklet_post'
        # include_resource_uri = False
        # limit = 10
        # default_format = "application/json"
        # object_class = ProxyStore
        # authorization = Authorization()

    def dehydrate(self, bundle):
        uris = []
        # filenames = []

        contents = bundle.data['contents']

        if contents:
            for content in contents:
                content_uri = '/api/v1/image/' + str(content.id) + '/'
                uris.append(str(content_uri))
                # content_file = str(content.data['image'])
                # filenames.append(content_file)
            bundle.data['contents'] = uris
            # bundle.data['filenames'] = filenames

        if not bundle.data['promo_image'] and bundle.data['contents'] and bundle.data['contents'][0]:
            bundle.data['promo_image'] = bundle.data['contents'][0]

        return bundle

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<id>[\d]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail")
        ]
