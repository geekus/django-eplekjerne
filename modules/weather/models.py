from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *
from eplekjerne.modules.models import *

class Weather(Module):
    pass

    def __unicode__(self):
        return self.filename

    class Meta(EpleMeta):
        db_table = 'eplekjerne_modules_weathers'
