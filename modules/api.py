from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.modules.models import *
from eplekjerne.api import EpleResourceMeta

from eplekjerne.sections.api import SectionResource
from eplekjerne.fetchers.api import FetcherResource

class ModuleResource(ModelResource):
    # section = fields.ToOneField('eplekjerne.sections.api.SectionResource', 'section', null=True, blank=True)
    fetcher = fields.ToOneField('eplekjerne.fetchers.api.FetcherResource', 'fetcher', null=True, blank=True)

    class Meta(EpleResourceMeta):
        queryset = Module.objects.all()
        resource_name = "module"

