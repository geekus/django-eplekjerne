from django.contrib.gis.db import models
from eplekjerne.models import EpleMeta

class Geo(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = models.CharField(max_length=50, blank=True, null=True)

    # GeoDjango-specific: a geometry field (MultiPolygonField), and
    # overriding the default manager with a GeoManager instance.
    # mpoly = models.MultiPolygonField()
    objects = models.GeoManager()

    # Returns the string representation of the model.
    # def __str__(self):              # __unicode__ on Python 2
    def __unicode__(self):
        return u'm'

    class Meta(EpleMeta):
        db_table = 'eplekjerne_geos'
