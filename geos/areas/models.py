from django.contrib.gis.db import models

from eplekjerne.models import EpleMeta
from eplekjerne.geos.models import Geo

class Area(Geo):
    geojson = models.PolygonField()

    objects = models.GeoManager() # NOTE: This will not be inherited from Geo.

    def __unicode__(self):
        return u'm'

    class Meta(EpleMeta):
        db_table = 'eplekjerne_geos_areas'

