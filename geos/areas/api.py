from tastypie.resources import ALL, ALL_WITH_RELATIONS
from tastypie.contrib.gis.resources import ModelResource

from eplekjerne.api import EpleResourceMeta
from eplekjerne.geos.areas.models import Area

class AreaResource(ModelResource):
    pass

    class Meta(EpleResourceMeta):
        resource_name = 'area'
        queryset = Area.objects.all()

        filtering = {
            'geojson': ALL
        }

# import urlparse

# # from django.contrib.auth.models import User
# # from tastypie.authorization import Authorization
# from tastypie import fields
# from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

# from eplekjerne.api import EpleResourceMeta
# from eplekjerne.contents.api import ContentResource
# from eplekjerne.contents.images.models import Image


# class ImageResource(ContentResource):

#     path = fields.FileField(attribute='image', blank=True)
#     aspect_ratio = fields.FloatField(attribute='aspect_ratio', readonly=True)
#     # filename = fields.CharField(attribute='filename', readonly=True)

#     class Meta(EpleResourceMeta):
#         queryset = Image.objects.all()
#         resource_name = 'image'
#         filtering = { 'image' : ALL }
#         # fields = ('image'),
#         # exclude = ('image')
#         # resource_name = 'bookmarklet_post'
#         # include_resource_uri = False
#         # limit = 10
#         max_limit = None
#         # default_format = "application/json"
#         # object_class = ProxyStore
#         # authorization = Authorization()
