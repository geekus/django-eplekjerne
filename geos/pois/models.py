from django.contrib.gis.db import models

from eplekjerne.models import EpleMeta
from eplekjerne.geos.models import Geo

class Poi(Geo):
    geojson = models.PointField()

    objects = models.GeoManager() # NOTE: This should be implicitly inherited.

    def __unicode__(self):
        return u'm'

    class Meta(EpleMeta):
        db_table = 'eplekjerne_geos_pois'

