from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *
from eplekjerne.sitelayouts.models import *
from eplekjerne.sections.models import *
from eplekjerne.contents.models import *

class Sitearea(models.Model):

    title = models.CharField(max_length=255, blank=True, null=True)
    order = models.IntegerField(blank=True, null=True)
    default = models.BooleanField(default=False)
    slug = models.CharField(max_length=255, blank=True, null=True)
    layout = models.ManyToManyField('Sitelayout', through="SiteareaHasSitelayout", related_name="contents")

    class Meta(EpleMeta):

        db_table = 'eplekjerne_siteareas'



class SiteareaHasSitelayout(models.Model):

    sitearea = models.ForeignKey('Sitearea')
    sitelayout = models.ForeignKey('Sitelayout')
    section = models.ForeignKey('Section', blank=True, null=True)
    # contenttype = models.ForeignKey('Contenttype', blank=True, null=True)
    content = models.ForeignKey('Content', blank=True, null=True)

    class Meta(EpleMeta):

        db_table = 'eplekjerne_siteareas_has_sitelayouts'
