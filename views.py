from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

from django import forms

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)

# Create your views here.

def index(request):
    if not request.user.is_authenticated():
        # return redirect('/login/?next=%s' % request.path)
        form = NameForm()
        context = {'latest_question_list': 'latest_question_list', 'form': form}
        return render(request, 'eplekjerne/login.html', context)
    else:
        context = {'latest_question_list': 'latest_question_list'}
        return render(request, 'eplekjerne/index.html', context)


def login_view(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/admin/')

            else:
                pass
                # Return a 'disabled account' error message
        else:
            pass
            # Return an 'invalid login' error message.
    else:
        return render(request, 'eplekjerne/login.html')


def logout_view(request):
    logout(request)
    return redirect('/')
