from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from eplekjerne.fetchers.models import *
from eplekjerne.api import EpleResourceMeta
from eplekjerne.tags.api import TagResource
from eplekjerne.contents.api import ContentResource

class FetcherResource(ModelResource):

    # sections = fields.ToManyField(SectionResource, 'sections', null=True, blank=True, related_name='contents')
    # section = fields.ToOneField('eplekjerne.sections.api.SectionResource', 'section', null=True, blank=True)
    # sections = fields.ToManyField('eplekjerne.sections.api.SectionResource', 'sections', null=True, blank=True, related_name='contents')

    # M2M
    tags = fields.ToManyField('eplekjerne.tags.api.TagResource', 'tags', null=True, blank=True, related_name='fetchers')
    # contents = fields.ToManyField('eplekjerne.contents.api.ContentResource', 'contents', readonly=True, null=True, blank=True, full=True)
    contents = fields.ListField(readonly=True, null=True, blank=True, attribute="contents")

    class Meta(EpleResourceMeta):
        queryset = Fetcher.objects.all()
        resource_name = "fetcher"

    def dehydrate(self, bundle):
        arr = []
        extras = []

        contents = bundle.data['contents']

        if contents:
            for content in contents:
                cn = '/api/v1/content/' + str(content.id) + '/'
                arr.append(str(cn))

            bundle.data['contents'] = arr

        return bundle
