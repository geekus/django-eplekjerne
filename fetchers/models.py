from django.db import models
from django.utils.text import slugify
from django.db.models.loading import get_model

from eplekjerne.models import *
from eplekjerne.contents.models import Content
from eplekjerne.contents.posts.models import Post
from eplekjerne.contents.pages.models import Page
from eplekjerne.contents.images.models import Image

class Fetcher(models.Model):

    CONTENT_TYPES = (
        ('Bilde', 'Image'),
        ('Post', 'Post'),
        ('Side', 'Page'),
    )

    title = models.CharField(max_length=255, blank=True)
    limit = models.IntegerField(max_length=10, null=True)
    offset = models.IntegerField(max_length=10, null=True)
    order = models.CharField(max_length=255, blank=True) # ASC or DESC
    order_by = models.CharField(max_length=255, blank=True) # Content fields
    reorder = models.CommaSeparatedIntegerField(max_length=255, blank=True) # Comma separated list of IDs
    from_date = models.DateTimeField(null=True)
    to_date = models.DateTimeField(null=True)
    tags = models.ManyToManyField('Tag', related_name='fetchers', db_table='eplekjerne_fetchers_has_tags')
    content_type = models.CharField(max_length=255, choices=CONTENT_TYPES)
    section = models.ForeignKey('Section', blank=True, null=True)

    def _get_contents(self):
        model = get_model('eplekjerne', self.content_type)

        # if self.tags:
        tag_ids = self.tags.values_list('id', flat=True)
        tag_ids = [int(tag_id) for tag_id in tag_ids]
        all_contents = model.objects.filter(tags__pk__in=tag_ids).distinct()

        # Get a flat list of all content IDs
        content_ids = all_contents.values_list('id', flat=True)
        content_ids = [int(item) for item in content_ids] # Make sure all items are integers

        if self.reorder:
        # Build a list of IDs as int's based on reorder comma separated string
            id_list = [int(id) for id in self.reorder.split(',')]

            extras = []
            for content in all_contents:
                if content.id not in id_list:
                    extras.append(content)

            # Build a new ordered ID list with only existing content IDs
            content_ids_ordered = []
            for id in id_list:
                if id in content_ids:
                    content_ids_ordered.append(id)

            objects = dict([(obj.id, obj) for obj in all_contents])
            sorted_objects = [objects[id] for id in content_ids_ordered]

            # Append extras that was not included in the ordered list
            if extras:
                sorted_objects.extend(extras)

            all_contents = sorted_objects

        return all_contents

    def _get_contents_(self):

        model = get_model('eplekjerne', self.content_type)

        # all_contents = model.objects.all()



        # if self.order_by:
        #     all_contents.order_by(self.order_by)
        # else:
        #     all_contents.order_by('published', 'created')

        # if self.tags:
        #     tag_ids = self.tags.values_list('id', flat=True)
        #     tag_ids = [int(tag_id) for tag_id in tag_ids]
        #     all_contents = model.objects.filter(tags__pk__in=tag_ids).distinct()

        #     # Get a flat list of all content IDs
        #     content_ids = all_contents.values_list('id', flat=True)
        #     content_ids = [int(item) for item in content_ids] # Make sure all items are integers

        # # params = {

        # # }

        offset = 0
        if self.offset:
            offset = self.offset
        print 'offset'
        print offset

        limit = None
        if self.limit:
            limit = offset + self.limit
        print 'limit'
        print limit


        # # model.objects.all()

        # if self.reorder:
        # # Build a list of IDs as int's based on reorder comma separated string
        #     id_list = [int(id) for id in self.reorder.split(',')]

        #     extras = []
        #     for content in all_contents:
        #         if content.id not in id_list:
        #             extras.append(content)

        #     # Build a new ordered ID list with only existing content IDs
        #     content_ids_ordered = []
        #     for id in id_list:
        #         if id in content_ids:
        #             content_ids_ordered.append(id)

        #     objects = dict([(obj.id, obj) for obj in all_contents])
        #     sorted_objects = [objects[id] for id in content_ids_ordered]

        #     # Append extras that was not included in the ordered list
        #     if extras:
        #         sorted_objects.extend(extras)

        #     all_contents = sorted_objects
        # contents = model.objects.order_by('title')

        order_by = 'id'
        if self.order_by:
            if self.order == 'DESC':
                order = '-'
            else:
                order = ''
            order_by = order + str(self.order_by)

        contents = model.objects.order_by(order_by).filter()[offset:limit]

        return contents

    contents = property(_get_contents_)

    class Meta(EpleMeta):
        db_table = 'eplekjerne_fetchers'
