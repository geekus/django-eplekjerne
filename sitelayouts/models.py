from django.db import models
from django.utils.text import slugify

from eplekjerne.models import *

class Sitelayout(models.Model):

    title = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    modules = models.ManyToManyField('Module', through='SitelayoutHasModule') # related_name='sitelayouts'

    class Meta(EpleMeta):

        db_table = 'eplekjerne_sitelayouts'



class SitelayoutHasModule(models.Model):

    sitelayout = models.ForeignKey('Sitelayout')
    module = models.ForeignKey('Module')
    order = models.IntegerField(null=True)

    class Meta(EpleMeta):
        db_table = 'eplekjerne_sitelayouts_has_modules'
