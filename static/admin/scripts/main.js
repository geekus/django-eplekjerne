(function() {

var Admin = window.Admin = Ember.Application.create();

/* Order and include as you please. */


})();

(function() {

Ember.Handlebars.helper('image', function(value, options) {
  var version = options;
  var filename = value.get('filename');
  var display = value || options;
  return new Ember.Handlebars.SafeString('<img src="/media/images/' + version + '/' + filename + '" />');
});


})();

(function() {

Ember.Handlebars.helper('or', function(value, options) {
  var display = value || options;
  return new Ember.Handlebars.SafeString(display);
});


})();

(function() {

Admin.CkEditorComponent = Ember.Component.extend({
  tagName: 'textarea',

  didInsertElement: function () {
    this.editor = CKEDITOR.replace(this.get('elementId'));
    this.editor.setData(this.get('value'));
    this.editor.on('change', this.onEditorChange, this);
  },

  willDestroyElement: function () {
    this.editor.destroy();
  },

  onEditorChange: function (e) {
    var editorValue = this.editor.getData();
    this.set('value', editorValue);
  }

});


})();

(function() {

Admin.FileUploadComponent = Ember.Component.extend({

  tagName: 'div',
  classNames: [''],

  didInsertElement: function () {
    // Ember.run.scheduleOnce('afterRender', this, 'doRender');
    var me = this;
    $('#fileupload').fileupload({
      url: '/upload/image/',
      // url: '/contents/files.json',
      dataType: 'json',
      done: function (e, data) {
        // $.each(data.result.files, function (index, file) {
        //   $('<p/>').text(file.name).appendTo(document.body);
        // });
      },
      complete: $.proxy(function (jqxhr, status) {
        var me = this;
        var list = me.get('list');

        var id = jqxhr.responseJSON.id;
        // debugger;
        list.addImage(id);
      }, this)
    });
  },

  willDestroyElement: function () {
    var me = this;
    // me.doDestroy();
  }

});


})();

(function() {

Admin.Select2TagComponent = Ember.Component.extend({

  tagName: 'input',
  classNames: ['select2'],

  // Source containing all tags available
  source: null,

  // Source tags
  sourceTags: [],

  // Target being tagged
  target: null,

  // Target tags
  targetTags: [],

  // Autosave target when tags selection is changed
  autosave: false,

  didInsertElement: function () {
    this.sourceObserver();
    this.targetObserver();
    Ember.run.scheduleOnce('afterRender', this, 'renderSelect2', true);
  },

  willDestroyElement: function () {
    this.destroySelect2();
  },

  renderSelect2: function (initial) {
    // console.log('select2-tag:renderSelect2');
    if (!initial) {
      this.destroySelect2();
    }

    var options = {
      width: '100%',
      tags: this.get('sourceTags')
    };

    this.$().select2(options).on('change', $.proxy(this.onSelect2Change, this));
    this.updateSelect2Value();
  }.observes('sourceTags.@each'), // Select2 needs to be re-rendered to update available tags

  destroySelect2: function () {
    this.$().select2('destroy');
  },

  updateSelect2Value: function () {
    var targetTags = this.get('targetTags');
    this.$().select2('val', targetTags);

  }.observes('targetTags.@each'),

  targetObserver: function () {
    var target = this.get('target.tags');
    var targetTags = (!!target && target.get('length')) ? target.mapBy('title') : [];
    this.set('targetTags', targetTags);
  }.observes('target.tags.@each.title'),

  sourceObserver: function () {
    var source = this.get('source');
    var sourceTags = (source.get('length')) ? source.mapBy('title') : [];
    this.set('sourceTags', sourceTags);
  }.observes('source.@each.title'),

  onSelect2Change: function (e) {
    var tag = '';

    if (!!this.e && this.e === e) {
      return; // TODO: This should be solved in a better way. Each time the select2 plugin is reRendered, a new change event handler is assignd, and this causes the onChange event to happen x times.
    }

    this.e = e; // Store this event to compare the next triggered event to the last. (Event being fired multiple events issue, see comment above)

    if (!!e.added) {
      tag = e.added.text;
      this.addedTag(tag);
    } else if (!!e.removed) {
      tag = e.removed.text;
      this.removedTag(tag);
    }

    if (this.get('autosave') === true) {
      try {
        this.get('target').save();
      } catch (e) {
        console.error(e);
      }
    }

  },

  addedTag: function (tag) {
    var target = this.get('target').get('tags'),
        targetHasTag = target.findBy('title', tag),
        source = this.get('source'),
        existingTag = source.findBy('title', tag);

    if (!existingTag) {

      existingTag = source.createTag(tag);
      existingTag.then($.proxy(function (tag) {

        var target = this.get('target'),
            targetTags = target.get('tags');

        targetTags.pushObject(tag);
        target.save();

      }, this));

    } else {
      if (!targetHasTag) {
        target.pushObject(existingTag);
      }
    }

  },

  removedTag: function (tag) {
    var target = this.get('target');
    target.removeTag(tag);
  }

});


})();

(function() {

Admin.NavTabComponent = Ember.Component.extend({
  tagName: 'li',

  classNames: ['nav-tab'],

  classNameBindings: ['isActive:active'],

  isActive: function() {
    return this.get('paneId') === this.get('parentView.activePaneId');
  }.property('paneId', 'parentView.activePaneId'),

  click: function() {
    this.get('parentView').setActivePane(this.get('paneId'));
  }

});

Admin.TabPaneComponent = Ember.Component.extend({
  classNames: ['tab-pane'],

  classNameBindings: ['isActive:active'],

  isActive: function() {
    return this.get('elementId') === this.get('parentView.activePaneId');
  }.property('elementId', 'parentView.activePaneId'),

  didInsertElement: function() {
    this.get('parentView.panes').pushObject({paneId: this.get('elementId'), name: this.get('name')});

    if (this.get('parentView.activePaneId') === null) {
      this.get('parentView').setActivePane(this.get('elementId'));
    }
  }

});

Admin.TabViewComponent = Ember.Component.extend({
  classNames: ['tab-view'],

  activePaneId: null,

  didInsertElement: function() {
    this.set('panes', []);
  },

  setActivePane: function(paneId) {
    if (this.get('activePaneId') !== null) {
      if (paneId !== this.get('activePaneId')) {
        this.set('activePaneId', paneId);
      }
    } else {
      this.set('activePaneId', paneId);
    }
  }

});


})();

(function() {

Admin.ApplicationController = Ember.Controller.extend({
  APP_NAME: 'eplekjerne'
});


})();

(function() {

Admin.GeoController = Ember.ObjectController.extend({

  actions: {
    save: function () {
      this._save();
    },

    // show: function () {
    //   // debugger;
    //   var marker = this.get('content.model.marker');
    //   // debugger;

    //   // var marker = this.content;
    //   marker.openPopup();
    // }
  },

  _save: function () {
    var model = this.get('model.model'); // TODO: Why double model?
    model.save().then(function () { console.log('saved!')});
    // debugger;
  }

});

Admin.GeosController = Ember.ArrayController.extend({

  needs: ['areas', 'pois'],

  itemController: 'geo',

  actions: {
    addPoi: function () {
      var poi = this.store.createRecord('poi');
      poi.set('name', 'whatup?');
      poi.set('point', {'type': 'Point', 'coordinates': [102.0, 0.5]});
      poi.save();
    }
  },

  createPoi: function (latLng) {
    var poi = this.store.createRecord('poi');
    poi.set('geojson', {type: 'Point', coordinates: [latLng.lng, latLng.lat]});
    poi.save().then(function () {
      console.log('poi is saved!');
    });
  },

  createArea: function (latLngs) {
    var area = this.store.createRecord('area');
    var geoJson = this.latLngsToGeoJson(latLngs);

    area.set('geojson', geoJson);

    area.save().then(function () {
      console.log('area is saved!');
    });
  },

  latLngToGeoJson: function (latLng) {

  },

  latLngsToGeoJson: function (latLngs) {
    var geoJson = {type: 'Polygon', coordinates: [[]]};

    latLngs.forEach(function (item, index, enumerable) {
      this.push([item.lng, item.lat]);
    }, geoJson.coordinates[0]);

    geoJson.coordinates[0].push(geoJson.coordinates[0][0]);

    return geoJson;
  },

  locations: function () {
    console.log('lager locations i geos controller');
    // debugger;

    var model = this.get('controllers.pois.model');

    var locations = [];

    model.forEach(function (item, index, enumerable) {

      var geoJson = item.get('geojson'),
          lat,
          lng;

      if (geoJson && geoJson.coordinates) {
        lat = geoJson.coordinates[1];
        lng = geoJson.coordinates[0];

        this.push({location: L.latLng(lat, lng), model: item});

      } else {
        console.error('error in geojson obj');
      }

    }, locations);

    return locations;

  }.property('controllers.pois.model.@each'),

  areas: function () {
    console.log('lager areas i geos controller');
    // debugger;

    var model = this.get('controllers.areas.model');

    var locations = [];

    model.forEach(function (item, index, enumerable) {

      var geoJson = item.get('geojson'),
          lat,
          lng;

      if (geoJson && geoJson.coordinates) {

        var coordinates = geoJson.coordinates[0],
            latLngs = [];

        for (var i = 0; i < coordinates.length-1; i++) {
          latLngs.push(L.latLng(coordinates[i].reverse()));
        }

        lat = geoJson.coordinates[1];
        lng = geoJson.coordinates[0];

        // this.push({location: L.polygon(latLngs)});
        this.push(latLngs);

      } else {
        console.error('error in geojson obj');
      }

    }, locations);

    console.log(locations);

    return locations;

  }.property('controllers.areas.model.@each')

});


})();

(function() {

Admin.AreaController = Admin.GeoController.extend({

  actions: {
    save: function () {
      this._save();
    }
  },

  _save: function () {
    var model = this.get('model'); // TODO: Why double model?
    model.save().then(function () { console.log('saved!')});
    // debugger;
  }

});


Admin.AreasController = Admin.GeosController.extend();


})();

(function() {

Admin.ContentController = Ember.ObjectController.extend({

  needs: ['contentstates', 'sections', 'tags'],

  actions: {
    save: function () { this.save(); },
    delete: function () { this.delete(); }
  },

  save: function () {
    var model = this.get('model');
    model.save();
  },

  delete: function () {
    var model = this.get('model');
    model.destroyRecord();
  },

  // NOTE: Move this method back to select2-tag component
  removeTag: function (tag) {
    var me = this,
        tags = me.get('tags'),
        tag = tags.findBy('title', tag);

    if (!!tag) {
      tags.removeObject(tag);
    }
  }

});


})();

(function() {

Admin.ContentstatesController = Ember.ArrayController.extend();


})();

(function() {

Admin.FetcherController = Ember.ObjectController.extend({

  needs: ['contentstates', 'sections', 'tags'],

  actions: {
    save: function () { this.save(); },
    delete: function () { this.delete(); }
  },

  save: function () {
    var model = this.get('model');
    model.save();
  },

  delete: function () {
    var model = this.get('model');
    model.destroyRecord();
  },

  // NOTE: Move this method back to select2-tag component
  removeTag: function (tag) {
    var me = this,
        tags = me.get('tags'),
        tag = tags.findBy('title', tag);

    if (!!tag) {
      tags.removeObject(tag);
    }
  }

});

Admin.FetchersEditController = Admin.FetcherController.extend({

  validParameters: Ember.A(['ids', 'contenttypes', 'tags', 'sections']),

  enabledParameters: Ember.A(),

  actions: {
    addParameterTags: function () {
      this.get('enabledParameters').pushObject('tags');
    },

    addParameterContenttype: function () {

    }
  },

  tagsEnabled: function () {
    return this.get('enabledParameters').indexOf('tags') > -1 ||  (this.get('tags.content.content') && this.get('tags.content.content').length);

  }.property('enabledParameters.@each', 'tags.@each')


});

Admin.FetchersController = Ember.ArrayController.extend({
  itemController: 'fetcher'
});


})();

(function() {

Admin.FileController = Admin.ContentController.extend({
});


})();

(function() {

Admin.FilesController = Ember.ArrayController.extend({

  actions: {


  }

});


})();

(function() {

Admin.ImageController = Admin.FileController.extend({

  // needs: ['contentstates', 'sections', 'tags'],

  smSquarePath: function () {
    var path = this.get('path'),
        smSquarePath;

    if (typeof path === 'string') {
      smSquarePath = path.replace('original', 'sm-square');
    }

    return smSquarePath;
  }.property('path')

});


})();

(function() {

Admin.ImagesController = Ember.ArrayController.extend({

  needs: ['tags'],

  itemController: 'image',
  actions: {},

  addImage: function (id) {
    this.store.find('image', id);
  },

  refresh: function () {
    // var m = this.store.find('image');
    // this.set('model', m); // NOTE: Should be using reload on the model but did not work?
    // debugger;
    // // this.get('model').reload();
    // var model = this.get('model');
    // model.reload();
  }

});


})();

(function() {

Admin.ModuleController = Ember.ObjectController.extend({

  needs: ['sections', 'tags', 'fetchers', 'fetcher'],

  actions: {
    save: function () { this.save(); },
    delete: function () { this.delete(); },
    addFetcher: function () { this.addFetcher(); }
  },

  save: function () {
    var module = this.get('model');
    var fetcher = module.get('fetcher');

    //
    if (fetcher) {
      fetcher.save().then(function () {
        module.save();
      });
    } else {
      module.save();
    }

  },

  delete: function () {
    var model = this.get('model');
    model.destroyRecord();
  },

  addFetcher: function () {
    var model = this.get('model'),
        fetcher = this.store.createRecord('fetcher'),
        fetcherController = this.get('controllers.fetcher');

    fetcherController.set('content', fetcher);

    model.save().then(function () {
      fetcher.save().then(function () {
        model.set('fetcher', fetcher);
      });
    });
  }

});

Admin.ModulesController = Ember.ArrayController.extend();


})();

(function() {

Admin.ListController = Admin.ModuleController.extend();

Admin.ListsEditController = Admin.ListController.extend();

Admin.ListsController = Admin.ModulesController.extend({

  itemController: 'list',

  actions: {
    create: function () {
      var module = 'list';
      this.store.createRecord(module).save().then(function () {

      });
    }
  }
});

Admin.ListsIndexController = Admin.ListsController.extend();


})();

(function() {

Admin.MapController = Ember.ObjectController.extend();


})();

(function() {

Admin.PoiController = Ember.ObjectController.extend({

  actions: {
    save: function () {
      this._save();
    },

    show: function () {
      // debugger;
      var marker = this.get('content.model.marker');
      // debugger;

      // var marker = this.content;
      marker.openPopup();
    }
  },

  _save: function () {
    var model = this.get('model.model'); // TODO: Why double model?
    model.save().then(function () { console.log('saved!')});
    // debugger;
  }

});


Admin.PoisController = Ember.ArrayController.extend({

  // needs: ['tags'],

  itemController: 'poi',

  actions: {
    // addPoi: function () {
    //   var poi = this.store.createRecord('poi');
    //   poi.set('name', 'whatup?');
    //   poi.set('point', {'type': 'Point', 'coordinates': [102.0, 0.5]});
    //   poi.save();
    // }
  },

  // createPoi: function (latLng) {
  //   var poi = this.store.createRecord('poi');
  //   poi.set('geojson', {type: 'Point', coordinates: [latLng.lng, latLng.lat]});
  //   poi.save().then(function () {
  //     console.log('poi is saved!');
  //   });

  // },

  refresh: function () {
    // var m = this.store.find('image');
    // this.set('model', m); // NOTE: Should be using reload on the model but did not work?
    // debugger;
    // // this.get('model').reload();
    // var model = this.get('model');
    // model.reload();
  }

});


})();

(function() {

Admin.PostController = Admin.ContentController.extend({

  needs: ['contentstates', 'sections', 'tags', 'images'],

  actions: {},

  // addTag: function (tag) {
  //   if(!tagA){
  //     tagA = this.source.createTag(tag);
  //   }

  //   if (!target.findBy('title', tag)) {
  //     target.pushObject(tagA);
  //   }
  // },


});


Admin.PostsController = Ember.ArrayController.extend({

  actions: {}

});


Admin.PostsEditController = Admin.PostController.extend();


Admin.PostsIndexController = Admin.PostsController.extend({
  actions: {}
});

Admin.PostsNewController = Admin.PostController.extend({

  actions: {

  }

});


})();

(function() {

Admin.SectionController = Ember.ObjectController.extend({

  actions: {
    save: function () {
      var model = this.get('model');
      model.save();
    }
  },

  // This is for using in has many sections checklist
  selected: function () {

    var model = this.get('model.content');
    var postSections = this.get('parentController.sections.content');
    var selected = (!!postSections && postSections.contains(model));

    return selected;

  }.property('parentController.sections.content.@each'),

  // This is for using in has many sections checklist
  selectedChanged: function () {

    var model = this.get('model.content'),
        id = model.get('id');

    var postSections = this.get('parentController.model.sections');

    if (this.get('selected')) {
      if (postSections.indexOf(model) < 0) {
        postSections.pushObject(model);
      }
    } else {
      postSections.removeObject(model);
    }

  }.observes('selected')

});


})();

(function() {

Admin.SectionsController = Ember.ArrayController.extend({

  needs: ['section'],

  itemController: 'section',

  // onContentChange: function(){
  //   console.log('model of sectionscontroller changed');
  // }.observes('model'),

  allowedHomeSections: function(){
    var me = this,
        allSections = me.get('model');

    var allowedHomeSections = allSections.filter(function(item, index, enumerable){
      return item.get('title') != 'Home';
    });

    return allowedHomeSections;

  }.property('model.@each')

});


})();

(function() {

Admin.SectionsIndexController = Ember.ObjectController.extend({
  actions: {
    create: function () {
      this.store.createRecord('section').save().then(function () {

      });
    }
  },
});


})();

(function() {

/**
 * Object
 */

Admin.SlideshowController = Admin.ModuleController.extend();

Admin.SlideshowsEditController = Admin.SlideshowController.extend();

Admin.SlideshowsNewController = Admin.SlideshowController.extend();


/**
 * Array
 */

Admin.SlideshowsController = Ember.ArrayController.extend({
  itemController: 'slideshow'
});

Admin.SlideshowsIndexController = Admin.SlideshowsController.extend();


})();

(function() {

Admin.TagsController = Ember.ArrayController.extend({

  createTag: function (title) {

    if (!this.findBy('title', title)) {
      var tag = this.store.createRecord('tag', {
        title: title,
        slug: title
      });
      return tag.save();
    }

  }

});


})();

(function() {

// Admin.Store = DS.Store.extend();


// Admin.Store = DS.Store.extend({
//   revision: 12,
//   adapter: DS.RESTAdapter.extend({
//     host: 'http://bonesmag.local',
//     namespace: 'api/v1'
//   })
// });

// Admin.ApplicationAdapter = DS.FixtureAdapter;

Admin.ApplicationAdapter = DS.RESTAdapter.extend({
  host: 'http://localhost:9000',
  namespace: 'api/v1'
  // buildURL: function(type, id) {
  //   var url = this._super(type, id);
  //   return '/contents' + url + ".json";
  // }
});


Admin.ApplicationAdapter = DS.DjangoTastypieAdapter.extend({});
Admin.ApplicationSerializer = DS.DjangoTastypieSerializer.extend({
  extractArray: function(store, primaryType, payload) {
    // debugger;
    var records = [];
    var self = this;
    payload.objects.forEach(function (hash) {
      self.extractEmbeddedFromPayload(store, primaryType, hash);
      records.push(self.normalize(primaryType, hash, primaryType.typeKey));
    });
    return records;
  },

});

// Admin.ApplicationSerializer = DS.RESTSerializer.extend({

//   extractArray: function(store, type, payload, id, requestType) {
//     // debugger;

//     // var posts = payload._embedded.post;
//     // var comments = [];
//     // var postCache = {};

//     // posts.forEach(function(post) {
//     //   post.comments = [];
//     //   postCache[post.id] = post;
//     // });

//     // payload._embedded.comment.forEach(function(comment) {
//     //   comments.push(comment);
//     //   postCache[comment.post_id].comments.push(comment);
//     //   delete comment.post_id;
//     // }

//     // payload = { comments: comments, posts: payload };

//     // debugger;
//     payload = payload.objects;

//     return this._super(store, type, payload, id, requestType);

//   }

// });

// DS.RESTAdapter.reopen({
//   headers: {
//     "API_KEY": "secret key",
//     "ANOTHER_HEADER": "Some header value"
//   }
// });

// // Admin.ApplicationAdapter = DS.RESTAdapter;

// DS.RESTAdapter.reopen({
//   namespace: 'api/1'
// });

// Admin.ApplicationSerializer = DS.JSONSerializer.extend({
//   serialize: function(record, options) {
//     deb
//     var json = {};

//     record.eachAttribute(function(name) {
//       json[serverAttributeName(name)] = record.get(name);
//     })

//     record.eachRelationship(function(name, relationship) {
//       if (relationship.kind === 'hasMany') {
//         json[serverHasManyName(name)] = record.get(name).mapBy('id');
//       }
//     });

//     if (options.includeId) {
//       json.ID_ = record.get('id');
//     }

//     return json;
//   }
// });

// function serverAttributeName(attribute) {
//   return attribute.underscore().toUpperCase();
// }

// function serverHasManyName(name) {
//   return serverAttributeName(name.singularize()) + "_IDS";
// }
// ```

// This serializer will generate JSON that looks like this:

// ```javascript
// {
//   "TITLE": "Rails is omakase",
//   "BODY": "Yep. Omakase.",
//   "COMMENT_IDS": [ 1, 2, 3 ]
// }
// ```


})();

(function() {

Admin.Content = DS.Model.extend({
  slug: DS.attr('string'),
  created: DS.attr('string'),
  modified: DS.attr('string'),
  tags: DS.hasMany('tag', {async: true})
});


})();

(function() {

Admin.Contentstate = DS.Model.extend({
  title: DS.attr('string')
});

// Admin.Contentstate.FIXTURES = [];


})();

(function() {

Admin.Fetcher = DS.Model.extend({
  title: DS.attr('string'),
  reorder: DS.attr('string'),
  types: DS.attr('string'),
  tags: DS.hasMany('tag', {async: true}),
  modules: DS.hasMany('module', {async: true}),
  contents: DS.hasMany('image', {async: true}) // NOTE: This MUST be fixed. Relation should be to content OR set dynamically
});


})();

(function() {

Admin.File = DS.Model.extend({
  title: DS.attr('string'),
  filename: DS.attr('string')
});

Admin.File.FIXTURES = [];


})();

(function() {

Admin.PointTransform = DS.Transform.extend({
  deserialize: function(serialized) {
    return serialized;
  },
  serialize: function(deserialized) {
    return deserialized;
  }
});

Admin.Geo = DS.Model.extend({
  name: DS.attr('string')
});

Admin.Area = Admin.Geo.extend({
  geojson: DS.attr('point')
});

Admin.Poi = Admin.Geo.extend({
  geojson: DS.attr('point')
});


})();

(function() {

Admin.Image = DS.Model.extend({

  // Inherited from Content
  title: DS.attr('string'),
  tags: DS.hasMany('tag', {async: true}),
  created: DS.attr('date'),
  modified: DS.attr('date'),
  publish_start: DS.attr('date'),
  publish_end: DS.attr('date'),

  // Image specific fields
  path: DS.attr('string'),
  image: DS.attr('string'),
  center: DS.attr('string'),
  filename: DS.attr('string'),
  width: DS.attr('number'),
  height: DS.attr('number'),

});
Admin.Image.FIXTURES = [];


})();

(function() {

Admin.Module = DS.Model.extend({
  title: DS.attr('string'),
  slug: DS.attr('string'),
  description: DS.attr('string'),
  fetcher: DS.belongsTo('fetcher'),
  contents: DS.hasMany('content', {async: true})
});

Admin.List = Admin.Module.extend({
});

Admin.Slideshow = Admin.Module.extend({
});


})();

(function() {

Admin.Post = DS.Model.extend({
  title: DS.attr('string'),
  slug: DS.attr('string'),
  lead: DS.attr('string'),
  body: DS.attr('string'),
  created_by: DS.belongsTo('user'),
  section: DS.belongsTo('section', {async: true}),
  sections: DS.hasMany('section', {async: true}),
  tags: DS.hasMany('tag', {async: true})
});


})();

(function() {

Admin.Role = DS.Model.extend({
  title: DS.attr('string'),
  slug: DS.attr('string')
});

Admin.Role.FIXTURES = [
  {
    id: 1,
    title: 'Administrators'
  },
  {
    id: 2,
    title: 'Members'
  },
  {
    id: 3,
    title: 'Users'
  }
];


})();

(function() {

Admin.Section = DS.Model.extend({
  posts: DS.hasMany('post', {inverse: 'sections'}),
  title: DS.attr('string'),
  slug: DS.attr('string')
});

// Admin.Section.FIXTURES = [
//   {
//     id: 1,
//     title: 'Home'
//   },
//   {
//     id: 2,
//     title: 'Fashion'
//   },
//   {
//     id: 3,
//     title: 'Design'
//   }
// ];


})();

(function() {

Admin.Slide = DS.Model.extend({
  title: DS.attr('string'),
  slideshow: DS.belongsTo('slideshow')
});


})();

(function() {

Admin.Tag = DS.Model.extend({
  posts: DS.hasMany('post'),
  title: DS.attr('string'),
  slug: DS.attr('string')
});


})();

(function() {

Admin.User = DS.Model.extend({
  username: DS.attr('string'),
  posts: DS.hasMany('post')
});


})();

(function() {

Admin.ApplicationRoute = Ember.Route.extend({
  // admittedly, this should be in IndexRoute and not in the
  // top level ApplicationRoute; we're in transition... :-)
  model: function () {

    this.store.push('user', {
      id: 1,
      username: 'geekus'
    });

    this.store.push('contentstate', {
      id: 1,
      title: 'draft'
    });

    this.store.push('contentstate', {
      id: 2,
      title: 'published'
    });

    this.store.push('contentstate', {
      id: 3,
      title: 'deleted'
    });

  },

  setupController: function(controller, model) {
    this.controllerFor('images').set('model', this.store.find('image'));
    this.controllerFor('files').set('model', this.store.find('file'));
    this.controllerFor('sections').set('model', this.store.find('section'));
    this.controllerFor('tags').set('model', this.store.find('tag'));
  },

  renderTemplate: function() {
    this.render('application');
  }
});


})();

(function() {

Admin.ImagesIndexRoute = Ember.Route.extend({

  model: function(){
    return this.store.find('image');
  },

  setupController: function(controller, model) {
     controller.set('model', model);
  },

  renderTemplate: function() {
    console.log('rendering image index template');
    this.render('images/index');

    this.render('images/list', {   // the template to render
      into: 'images/index',                // the route to render into
      outlet: 'list',              // the name of the outlet in the route's template
      controller: 'images'        // the controller to use for the template
    });

  }

});


})();

(function() {

Admin.ListsIndexRoute = Ember.Route.extend({

  model: function () {
    return this.store.find('list');
  },

  setupController: function (controller, model) {
    var listsController = this.controllerFor('lists')
    listsController.set('model', model);
  },

  renderTemplate: function () {
    this.render('lists/index');

    this.render('lists/list', {
      into: 'lists/index',
      outlet: 'list',
      controller: 'lists'
    });

  }

});



Admin.ListsEditRoute = Ember.Route.extend({

  model: function (params) {
    return this.store.find('list', params.list_id);
  },

  setupController: function (controller, model) {
    controller.set('model', model);
    this.controllerFor('fetchers-edit').set('model', model.get('fetcher'));
  },

  renderTemplate: function () {
    this.render('lists/edit');

    this.render('fetchers/edit', {
      into: 'lists/edit',
      outlet: 'fetcher',
      controller: 'fetchers-edit'
    });

  }

});


})();

(function() {

Admin.MapsRoute = Ember.Route.extend({

  model: function () {
    // return this.store.find('poi');
    return Ember.Object.create({
      pois: this.store.find('poi'),
      areas: this.store.find('area')
    });
  },

  setupController: function (controller, model) {
    var poisController = this.controllerFor('pois');
    poisController.set('model', model.pois);

    var areasController = this.controllerFor('areas');
    areasController.set('model', model.areas);

    // var geosController = this.controllerFor('geos');
    // geosController.set('model', model);
    // debugger;
     // controller.set('model', model);
  },

  renderTemplate: function () {

    console.log('rendering map index template');
    this.render('maps/index');

    this.render('maps/toolbar', {   // the template to render
      into: 'maps/index',                // the tpm to render into
      outlet: 'toolbar',              // the name of the outlet in the route's template
      // controller: 'images'        // the controller to use for the template
    });

    this.render('maps/map', {   // the template to render
      into: 'application',                // the tpm to render into
      outlet: 'fullscreen',              // the name of the outlet in the route's template
      controller: 'geos'        // the controller to use for the template
    });

    this.render('maps/geos', {   // the template to render
      into: 'maps/map',                // the tpm to render into
      outlet: 'geos',              // the name of the outlet in the route's template
      controller: 'geos'        // the controller to use for the template
    });


  }

});


})();

(function() {

Admin.ModulesRoute = Ember.Route.extend({

  actions: {}

});


})();

(function() {

Admin.PoisIndexRoute = Ember.Route.extend({

  model: function(){
    return this.store.find('poi');
  },

  setupController: function(controller, model) {
     // controller.set('model', model);
     this.controllerFor('pois').set('model', model);
  },

  renderTemplate: function() {
    console.log('rendering poi index template');
    this.render('pois/index');

    this.render('pois/list', {   // the template to render
      into: 'pois/index',                // the route to render into
      outlet: 'list',              // the name of the outlet in the route's template
      controller: 'pois'        // the controller to use for the template
    });

  }

});


})();

(function() {

Admin.PostsEditRoute = Ember.Route.extend({

  model: function(params) {
    return this.store.find('post', params.post_id);
  },

  setupController: function(controller, model) {
    // var me = this,
        // store = me.store;
        // debugger;
    controller.set('model', model);
    // model.reload();

    // console.log('Setting up controller');

  },

  renderTemplate: function() {

    console.log('rendering post edit template');

    this.render('posts/edit');

    this.render('files/list', {   // the template to render
      into: 'posts/edit',                // the route to render into
      outlet: 'filelist',              // the name of the outlet in the route's template
      controller: 'files'        // the controller to use for the template
    });

    this.render('images/list', {   // the template to render
      into: 'posts/edit',                // the route to render into
      outlet: 'imagelist',              // the name of the outlet in the route's template
      controller: 'images'        // the controller to use for the template
    });

  }

});


})();

(function() {

Admin.PostsIndexRoute = Ember.Route.extend({

  model: function(){
    return this.store.find('post');
  },

  setupController: function(controller, model) {
     controller.set('model', model);
   },

  renderTemplate: function() {
    console.log('rendering post new template');

    this.render('posts/index');

  }

});


})();

(function() {

Admin.PostsNewRoute = Ember.Route.extend({

  model: function(params) {
    return this.store.createRecord('post');
  },

  setupController: function(controller, model) {
    // var me = this,
        // store = me.store;
        // debugger;
    var postController = this.controllerFor('post');
    postController.set('model', model);

    // model.reload();

    // console.log('Setting up controller');

  },

  renderTemplate: function() {

    console.log('rendering post edit template');

    this.render('posts/edit', {
      controller: 'post'
    });

    this.render('files/list', {   // the template to render
      into: 'posts/edit',                // the route to render into
      outlet: 'filelist',              // the name of the outlet in the route's template
      controller: 'files'        // the controller to use for the template
    });

    this.render('images/list', {   // the template to render
      into: 'posts/edit',                // the route to render into
      outlet: 'imagelist',              // the name of the outlet in the route's template
      controller: 'images'        // the controller to use for the template
    });

  }

});











// Admin.PostsNewRoute = Ember.Route.extend({

//   model: function(){
//     var me = this;
//     return me.store.createRecord('post');
//   },

//   setupController: function(controller, model) {
//     var me = this,
//         store = me.store,
//         allFiles = store.find('file'),
//         filesController = me.controllerFor('files');

//     controller.set('model', model);
//     filesController.set('content', allFiles);

//   },

//   renderTemplate: function() {
//     console.log('rendering post new template');

//     this.render('posts/new');

//     this.render('files/list', {   // the template to render
//       into: 'posts/new',                // the route to render into
//       outlet: 'filelist',              // the name of the outlet in the route's template
//       controller: 'files'        // the controller to use for the template
//     });


//   }

//   // renderTemplate: function() {
//   //   // this.render('favoritePost', {   // the template to render
//   //   //   into: 'posts',                // the route to render into
//   //   //   outlet: 'posts',              // the name of the outlet in the route's template
//   //   //   controller: 'blogPost'        // the controller to use for the template
//   //   // });

//   //   console.log('rendering template');

//   //   this.render('files/list', {
//   //     into: 'application',
//   //     outlet: 'filelist',
//   //     // controller: 'blogPost'
//   //   });
//   // }

// });


})();

(function() {

Admin.SectionsEditRoute = Ember.Route.extend({

  model: function (params) {
    return this.store.find('section', params.section_id);
  },

  setupController: function(controller, model) {
    // var me = this,
    //     store = me.store,
    //     allImages = store.find('image'),
    //     imagesController = me.controllerFor('images'),
    //     fetcher = model.get('fetcher'),
    //     fetcherController = me.controllerFor('fetcher');

    controller.set('model', model);
    // imagesController.set('model', allImages);
    // fetcherController.set('model', fetcher);

  },

  renderTemplate: function() {
    this.render('sections/edit');

    // this.render('slideshows/form', {   // the template to render
    //   into: 'slideshows/edit',                // the route to render into
    //   outlet: 'form',              // the name of the outlet in the route's template
    //   controller: 'slideshows.edit'        // the controller to use for the template
    // });

    // this.render('fetchers/tabs', {   // the template to render
    //   into: 'slideshows/edit',                // the route to render into
    //   outlet: 'fetcher',              // the name of the outlet in the route's template
    //   controller: 'fetcher'        // the controller to use for the template
    // });

    // this.render('fetchers/fetch-by-tags', {   // the template to render
    //   into: 'fetchers/tabs',                // the route, no, template to render into
    //   outlet: 'fetch-by-tags',              // the name of the outlet in the route's template
    //   controller: 'fetcher'        // the controller to use for the template
    // });

  }

});


})();

(function() {

(function (App) {
  App.SectionsIndexRoute = Ember.Route.extend({

    model: function(){
      // return this.store.find('section');
    },

    setupController: function(controller, model) {
      // Sections are already loaded in application route
    },

    renderTemplate: function() {
      this.render('sections/index');

      this.render('sections/list', {
        into: 'sections/index',
        outlet: 'list',
        controller: 'sections'
      });
    }

  });

})(Admin);


})();

(function() {

Admin.SlideshowsEditRoute = Ember.Route.extend({

  model: function (params) {
    return this.store.find('slideshow', params.slideshow_id);
  },

  setupController: function(controller, model) {
    var me = this,
        store = me.store,
        allImages = store.find('image'),
        imagesController = me.controllerFor('images'),
        fetcher = model.get('fetcher'),
        fetcherController = me.controllerFor('fetcher');

    controller.set('model', model);
    imagesController.set('model', allImages);
    fetcherController.set('model', fetcher);

  },

  renderTemplate: function() {
    this.render('slideshows/edit');

    this.render('slideshows/form', {   // the template to render
      into: 'slideshows/edit',                // the route to render into
      outlet: 'form',              // the name of the outlet in the route's template
      controller: 'slideshows.edit'        // the controller to use for the template
    });

    this.render('fetchers/tabs', {   // the template to render
      into: 'slideshows/edit',                // the route to render into
      outlet: 'fetcher',              // the name of the outlet in the route's template
      controller: 'fetcher'        // the controller to use for the template
    });

    this.render('fetchers/fetch-by-tags', {   // the template to render
      into: 'fetchers/tabs',                // the route, no, template to render into
      outlet: 'fetch-by-tags',              // the name of the outlet in the route's template
      controller: 'fetcher'        // the controller to use for the template
    });

  }

});


})();

(function() {

Admin.SlideshowsIndexRoute = Ember.Route.extend({

  model: function(){
    return this.store.find('slideshow');
  },

  setupController: function(controller, model) {
     controller.set('model', model);
   },

  renderTemplate: function() {
    this.render('slideshows/index');
  }

});


})();

(function() {

Admin.SlideshowsNewRoute = Ember.Route.extend({

  model: function () {
    return this.store.createRecord('slideshow');
  },

  setupController: function(controller, model) {
    controller.set('model', model);

    // var store = this.store,
    //     allImages = store.find('image'),
    //     imagesController = this.controllerFor('images'),
    //     fetcher = model.get('fetcher'),
    //     fetcherController = this.controllerFor('fetcher');

    // controller.set('model', model);
    // imagesController.set('model', allImages);
    // fetcherController.set('model', fetcher);

  },

  renderTemplate: function() {
    this.render('slideshows/new');

    this.render('slideshows/form', {   // the template to render
      into: 'slideshows/new',                // the route to render into
      outlet: 'form',              // the name of the outlet in the route's template
      controller: 'slideshows.new'        // the controller to use for the template
    });

    this.render('fetchers/tabs', {   // the template to render
      into: 'slideshows/new',                // the route to render into
      outlet: 'fetcher',              // the name of the outlet in the route's template
      controller: 'fetcher'        // the controller to use for the template
    });

    this.render('fetchers/fetch-by-tags', {
      into: 'fetchers/tabs',
      outlet: 'fetch-by-tags',
      controller: 'fetcher'
    });

  }

});


})();

(function() {

Admin.FetchersFetchByTagsView = Ember.View.extend({

  didInsertElement: function () {
    var controller = this.get('controller');

    $('div.sortable').sortable({
      items: '> div.item',
      update: $.proxy(this.sortableUpdate, this)
    });
  },

  sortableUpdate: function (event, ui) {
    var arr = $('div[data-jqueryui="sortable-container"]').sortable('toArray', {attribute: 'data-id'});
    var controller = this.get('controller');
    controller.set('reorder', arr.join());
    controller.save();
  }

});


})();

(function() {

Admin.GeoListItemView = Ember.View.extend({

  classNames: ['item'],

  expanded: false,

  didInsertElement: function () {
    console.log('maps geo view');
  },

  actions: {
    edit: function () {
      this.set('expanded', true);
      // console.log('edit');
      // debugger;
    }
  }

});


})();

(function() {

Admin.TileLayer = EmberLeaflet.TileLayer.extend({
  tileUrl: 'http://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo2&zoom={z}&x={x}&y={y}',
  // options: {key: 'API-key', styleId: 997}
});

// StamenLayer = new L.StamenTileLayer("toner");

EmberLeaflet.DraggableMarkerLayer = EmberLeaflet.MarkerLayer.extend(EmberLeaflet.DraggableMixin, EmberLeaflet.PopupMixin, {

  _createLayer: function() {
    this._super();

    var marker = this,
        model = this.get('content.model');

    model.set('marker', marker);
  },

  popupContentBinding: 'content.model.name',
  events: ['dragstart'],
  dragend: function (e) {
    var model = this.get('content.model');
    var latLng = this.get('layer').getLatLng();

    model.set('geojson', {type: 'Point', coordinates: [latLng.lng, latLng.lat]}).save().then(function () {
      console.log('Model new location saved!');
    });
  }
});

EmberLeaflet.DraggableMarkerCollectionLayer = EmberLeaflet.MarkerCollectionLayer.extend({
  itemLayerClass: EmberLeaflet.DraggableMarkerLayer
});

Admin.AreasLayer = EmberLeaflet.MarkerCollectionLayer.extend({
  itemLayerClass: EmberLeaflet.PolygonLayer,
  contentBinding: 'controller.areas',
});

Admin.MarkerCollectionLayer = EmberLeaflet.DraggableMarkerCollectionLayer.extend({
  contentBinding: 'controller.locations',
});

Admin.MapsMapView = EmberLeaflet.MapView.extend({

  attributeBindings: ['style'],
  style: 'position: absolute; top: 50px; left: 0; right: 0; bottom: 0;',
  center: L.latLng(56.81374171570782, 17.734375),
  zoom: 5,
  childLayers: [
    EmberLeaflet.DefaultTileLayer,
    // Admin.TileLayer,
    Admin.MarkerCollectionLayer,
    Admin.AreasLayer
  ],

  // options: {
  //   drawControl: true
  // },
  events: [
    'draw:created',
    'draw:edited',
    'draw:deleted',
    'draw:drawstart',
    'draw:drawstop',
    'draw:editstart',
    'draw:editstop',
    'draw:deletestart',
    'draw:deletestop',
    'layeradd'
  ],

  layeradd: function (layer) {
    if (!!layer && !!layer.layer && !!layer.layer.options && !!layer.layer.options.stroke) {
      this.drawnItems.addLayer(layer.layer);
    }
  },

    didCreateLayer: function () {
      this._super();


      console.log('did create layer', this);
      var map = this.get('layer'),
          drawnItems = new L.FeatureGroup(),
          drawControl;

      this.set('drawnItems', drawnItems);
      map.addLayer(this.drawnItems);
      drawControl = new L.Control.Draw({
        edit: {
          featureGroup: this.drawnItems
        }
      });
      map.addControl(drawControl);
    },

    "draw:created": function (e) {
      var layer = e.layer,
          type = e.layerType,
          drawnItems = this.get('drawnItems'),
          bounds;

      var controller = this.get('controller');

      if (type === 'marker') {
        var latLng = layer.getLatLng();
        controller.createPoi(latLng);

      } else if (type === 'rectangle') {
        var latLngs = layer.getLatLngs();
        controller.createArea(latLngs);

      } else if (type === 'polygon') {
        var latLngs = layer.getLatLngs();
        controller.createArea(latLngs);
      }

      drawnItems.addLayer(layer);

      return this;
    },

    "draw:edited": function (e) {
      // debugger;
    }


});


})();

(function() {

// Admin.MapsView = EmberLeaflet.MapView.extend({});
Admin.MapsIndexView = Ember.View.extend({

  didInsertElement: function () {
    console.log('abc');
  },

});


})();

(function() {

(function (Admin) {

  Admin.Router.map(function () {
    // Add your routes here

    this.resource('maps', {path: '/maps'});

    this.resource('contents', {path: '/contents'}, function () {

      this.resource('posts', function() {
        this.route('new');
        this.route('edit', { path: 'edit/:post_id' });
      });

      this.resource('pages', function() {
        this.route('new');
        this.route('edit', { path: 'edit/:page_id' });
      });

      this.resource('pois', function() {});
      this.resource('images', function() {});

    });

    // Modules
    this.resource('modules', function () {

      // Slideshows
      this.resource('slideshows', function () {
        this.route('new');
        this.route('edit', { path: 'edit/:slideshow_id' });
      });

      // Content lists
      this.resource('lists', function () {
        this.route('new');
        this.route('edit', { path: 'edit/:list_id' });
      });
    });

    // Site stuff
    this.resource('sites', {path: 'sites'}, function () {
      this.resource('sections', function () {
        this.route('new');
        this.route('edit', { path: 'edit/:slideshow_id' });
      });
    });

  });

})(Admin);


})();
