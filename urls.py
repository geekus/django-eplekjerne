from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login

from eplekjerne.api import eplekjerne_api
from eplekjerne import views

urlpatterns = patterns('eplekjerne',
    url(r'^admin/', 'views.index', name='admin-index'),
    url(r'^upload/image/$', 'contents.images.views.upload', name='upload'),
    url(r'^login/$', 'views.login_view', name='eplekjerne.login'),
    url(r'^logout/$', 'views.logout_view', name="eplekjerne.logout"),
)

urlpatterns += patterns('',
    (r'^api/', include(eplekjerne_api.urls)),
)
